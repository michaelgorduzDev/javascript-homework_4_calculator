1. Описати своїми словами навіщо потрібні функції у програмуванні.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We need functions to make our code be more compact. Also, if we need to perform same action 
many times, we can just create function, which will automate this process.
Also, if we need to change some parameters - in function we can do it easily by changing
function arguments.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
2. Описати своїми словами, навіщо у функцію передавати аргумент.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We need arguments in function for making our function more universal. Instead of creating
new function we can just change arguments in current function.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
3. Що таке оператор return та як він працює всередині функції?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Return is what our function returning to us. For example, function counting (a, b , c) 
arguments will return sum of a+b+c;
After keyword "return" our function terminates and all further code after "return" will be
unreachable.